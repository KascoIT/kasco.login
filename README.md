# Login Structure

.Net Core Angular 5 Use Authentication IdentityServer 4 (SSO)

- Visual Studio 2017
- SQL Server 2017
- .NET Core 2.1
- ASP.NET Core 2.1
- Entity Framework Core 2.1
- C# 7.2
- SPA (Single Page Application)
- Angular 5.2.0
- Typescript 2.6.2
- HTML5
- CSS3
- SOLID Principles
- Dependency Injection
- Memory Caching
- Response Caching
- OPENID
- Identity Server 4
- Object Mapping
- Code Analysis (Ruleset for C# and TSLint for Typescript)

# Configulation
1. Go to Project Login.IdentityServer
2. Open appsettings.json
3. Change ConnectionStrings

# Run Visual Studio 2017
1. Install .NET Core 2.1 SDK: https://www.microsoft.com/net/download/windows.
2. Open solution folder in Visual Studio 2017.
3. Right Click Project Solution and click Property chosse Multiple Startup and change action of WEB & WebService to Start

Press Start to run and fun!
