"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var home_component_1 = require("./modules/home/home.component");
var counter_component_1 = require("./modules/counter/counter.component");
var fetch_data_component_1 = require("./modules/fetch-data/fetch-data.component");
var unauthorized_component_1 = require("./modules/unauthorized/unauthorized.component");
var authentication_guard_1 = require("./shared/guards/authentication.guard");
exports.ROUTES = [
    { path: '', component: home_component_1.HomeComponent, pathMatch: 'full' },
    { path: 'counter', component: counter_component_1.CounterComponent },
    { path: 'fetch-data', component: fetch_data_component_1.FetchDataComponent, canActivate: [authentication_guard_1.AuthenticationGuard] },
    { path: 'unauthorized', component: unauthorized_component_1.UnauthorizedComponent },
    { path: '', component: home_component_1.HomeComponent },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', redirectTo: '' }
];
//# sourceMappingURL=app.routes.js.map