import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { OidcSecurityService } from '../auth/services/oidc.security.service';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {

  constructor(private securityService: OidcSecurityService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let url = request.url;

    if (!url.startsWith("http")) {
      url = document.getElementsByTagName("base").item(0).href + url;
    }

    request = request.clone({
      setHeaders: { Authorization: `Bearer ${this.securityService.getToken()}` },
      url
    });

    return next.handle(request);
  }
}
