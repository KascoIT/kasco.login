import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { OidcSecurityService } from '../auth/services/oidc.security.service'

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private router: Router,
    private oidcSecurityService: OidcSecurityService) { }

  canActivate(): Observable<boolean> | boolean {

    return this.oidcSecurityService.getIsAuthorized().pipe(
      map((isAuthorized: boolean) => {

        if (isAuthorized) {
          return true;
        }

        this.router.navigate(['/unauthorized']);
        return false;
      }),
      take(1)
    );
  }
}

