export var AuthorizationState;
(function (AuthorizationState) {
    AuthorizationState["authorized"] = "authorized";
    AuthorizationState["forbidden"] = "forbidden";
    AuthorizationState["unauthorized"] = "unauthorized";
})(AuthorizationState || (AuthorizationState = {}));
//# sourceMappingURL=authorization-state.enum.js.map