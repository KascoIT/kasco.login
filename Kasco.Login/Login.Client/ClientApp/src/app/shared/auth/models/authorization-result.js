var AuthorizationResult = (function () {
    function AuthorizationResult(authorizationState, validationResult) {
        this.authorizationState = authorizationState;
        this.validationResult = validationResult;
    }
    return AuthorizationResult;
}());
export { AuthorizationResult };
//# sourceMappingURL=authorization-result.js.map