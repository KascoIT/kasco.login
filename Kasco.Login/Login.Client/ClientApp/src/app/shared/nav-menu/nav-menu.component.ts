import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from '../auth/services/oidc.security.service';
import { AuthorizationResult } from '../auth/models/authorization-result';
import { Subscription } from 'rxjs';
import { AuthorizationState } from '../auth/models/authorization-state.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  isExpanded = false;

  isAuthorizedSubscription: Subscription | undefined;
  isAuthorized = false;

  onChecksessionChanged: Subscription | undefined;
  checksession = false;

  constructor(public oidcSecurityService: OidcSecurityService,
    private router: Router)
  {
    if (this.oidcSecurityService.moduleSetup)
    {
      this.doCallbackLogicIfRequired();
    }
    else
    {
      this.oidcSecurityService.onModuleSetup.subscribe(() => {
        this.doCallbackLogicIfRequired();
      });
    }

    this.oidcSecurityService.onCheckSessionChanged.subscribe(
      (checksession: boolean) => {
        this.checksession = checksession;
      });

    this.oidcSecurityService.onAuthorizationResult.subscribe(
      (authorizationResult: AuthorizationResult) => {
        this.onAuthorizationResultComplete(authorizationResult);
      });
  }

  ngOnInit() {
    this.isAuthorizedSubscription = this.oidcSecurityService.getIsAuthorized().subscribe(
      (isAuthorized: boolean) => {
        this.isAuthorized = isAuthorized;
      });
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  logout() {
    this.oidcSecurityService.logoff();
  }

  refreshSession() {
    this.oidcSecurityService.authorize();
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }


  private doCallbackLogicIfRequired() {
    if (window.location.hash) {
      this.oidcSecurityService.authorizedCallback();
    }
  } 

  private onAuthorizationResultComplete(authorizationResult: AuthorizationResult)
  {
    if (authorizationResult.authorizationState === AuthorizationState.unauthorized)
    {
      if (window.parent)
      {
        // sent from the child iframe, for example the silent renew
        this.router.navigate(['/unauthorized']);
      }
      else {
        window.location.href = '/unauthorized';
      }
    }
  }


}
