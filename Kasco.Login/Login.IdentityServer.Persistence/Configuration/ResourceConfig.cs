﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Login.IdentityServer.Persistence.Configuration
{
    public class ResourceConfig
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api1", "API Server")
                {
                    ApiSecrets = { new Secret("secret".Sha256()) }
                }
            };
        }
    }
}
