﻿using Login.IdentityServer.Persistence.Model;
using Microsoft.EntityFrameworkCore;

namespace Login.IdentityServer.Persistence
{
    public class ConfigurationStoreContext: DbContext
    {
        public ConfigurationStoreContext(DbContextOptions options) : base(options)
        { }

        public DbSet<ClientEntity> Clients { get; set; }
        public DbSet<ApiResourceEntity> ApiResources { get; set; }
        public DbSet<IdentityResourceEntity> IdentityResources { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ClientEntity>().HasKey(m => m.ClientId);
            builder.Entity<ApiResourceEntity>().HasKey(m => m.ApiResourceName);
            builder.Entity<IdentityResourceEntity>().HasKey(m => m.IdentityResourceName);

            base.OnModelCreating(builder);
        }
    }
}
