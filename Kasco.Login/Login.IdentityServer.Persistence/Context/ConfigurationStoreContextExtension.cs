﻿using Login.IdentityServer.Persistence.Configuration;
using Login.IdentityServer.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Login.IdentityServer.Persistence
{
    public static class ConfigurationStoreContextExtension
    {
        public static void Seed(this ConfigurationStoreContext context)
        {
            SeedClient(context);
            SeedIdentityResource(context);
            SeedApiResource(context);
            context.SaveChanges();
        }

        static void SeedClient(ConfigurationStoreContext context)
        {
            var clients = ClientConfig.GetClients();

            List<ClientEntity> clientModel = new List<ClientEntity>();
            foreach (var client in clients.ToList())
            {
                var clientEntity = new ClientEntity
                {
                    Client = client
                };

                clientEntity.AddDataToEntity();
                clientModel.Add(clientEntity);
            }

            context.Clients.AddRange(clientModel);
        }

        static void SeedIdentityResource(ConfigurationStoreContext context)
        {
            var identityResources = ClientConfig.GetIdentityResources();
            List<IdentityResourceEntity> identityResourcesModel = new List<IdentityResourceEntity>();

            foreach (var identityResource in identityResources.ToList())
            {
                var identityResourceEntity = new IdentityResourceEntity
                {
                    IdentityResource = identityResource
                };

                identityResourceEntity.AddDataToEntity();
                identityResourcesModel.Add(identityResourceEntity);
            }

            context.IdentityResources.AddRange(identityResourcesModel);
        }

        static void SeedApiResource(ConfigurationStoreContext context)
        {
            var apiResources = ResourceConfig.GetApiResources();
            List<ApiResourceEntity> apiResourcesModel = new List<ApiResourceEntity>();

            foreach (var apiResource in apiResources.ToList())
            {
                var apiResourceEntity = new ApiResourceEntity
                {
                    ApiResource = apiResource
                };
                apiResourceEntity.AddDataToEntity();
                apiResourcesModel.Add(apiResourceEntity);
            }

            context.ApiResources.AddRange(apiResourcesModel);
        }
    }
}
