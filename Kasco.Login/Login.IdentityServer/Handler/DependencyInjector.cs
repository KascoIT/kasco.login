﻿using IdentityServer4.Services;
using IdentityServer4.Stores;
using Login.IdentityServer.Persistence.Store;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Login.IdentityServer.Handler
{
    public static class DependencyInjector
    {

        static IServiceProvider ServiceProvider { get; set; }

        static IServiceCollection Services { get; set; }

        public static void AddDbContext<T>(string connectionString) where T : DbContext
        {
            Services.AddDbContext<T>(options => options.UseSqlServer(connectionString));
            var context = GetService<T>();
            context.Database.EnsureCreated();
            context.Database.Migrate();
        }

        public static T GetService<T>()
        {
            Services = Services ?? RegisterServices();
            ServiceProvider = ServiceProvider ?? Services.BuildServiceProvider();
            return ServiceProvider.GetService<T>();
        }

        public static IServiceCollection RegisterServices()
        {
            return RegisterServices(new ServiceCollection());
        }

        public static IServiceCollection RegisterServices(IServiceCollection services)
        {
            Services = services;

            //services.AddTransient<IPersistedGrantStore, PersistedGrantStore>();

            Services.AddTransient<IProfileService, IdentityWithAdditionalClaimsProfileService>();

            Services.AddTransient<IClientStore, ClientStore>();
            Services.AddTransient<IResourceStore, ResourceStore>();

            return Services;
        }

    }
}
