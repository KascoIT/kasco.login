﻿using IdentityServer;
using Login.IdentityServer.Data;
using Login.IdentityServer.Handler;
using Login.IdentityServer.Models;
using Login.IdentityServer.Persistence;
using Login.IdentityServer.Persistence.Configuration;
using Login.IdentityServer.Persistence.Store;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Login.IdentityServer.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void AddDependencyInjectionCustom(this IServiceCollection services, IConfiguration configuration)
        {
            DependencyInjector.RegisterServices(services);

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            DependencyInjector.AddDbContext<ConfigurationStoreContext>(configuration.GetConnectionString("ConfigurationtConnection"));

            //Seed
            //DependencyInjector.GetService<ConfigurationStoreContext>().Seed();
        }

        public static void AddIdentityCustom(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
        }

        public static void AddIdentityServerCustom(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddResourceStore<ResourceStore>()
                .AddClientStore<ClientStore>()
                .AddAspNetIdentity<ApplicationUser>()
                .AddProfileService<IdentityWithAdditionalClaimsProfileService>();
        }
    }
}
