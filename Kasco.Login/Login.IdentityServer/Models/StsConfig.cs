﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.IdentityServer.Models
{
    public class StsConfig
    {
        public string StsServerIdentityUrl { get; set; }
        public string AngularClientUrl { get; set; }
        public string AngularClientIdTokenOnlyUrl { get; set; }
    }
}
