﻿// Copyright (c) Nuttakrit.A All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


namespace Login.IdentityServer.Quickstart.UI
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
