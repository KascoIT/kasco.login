import { Routes } from "@angular/router";
import { HomeComponent } from "./modules/home/home.component";
import { CounterComponent } from "./modules/counter/counter.component";
import { FetchDataComponent } from "./modules/fetch-data/fetch-data.component";
import { UnauthorizedComponent } from "./modules/unauthorized/unauthorized.component";

import { AuthenticationGuard } from './shared/guards/authentication.guard';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'counter', component: CounterComponent },
  { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthenticationGuard] },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: '', component: HomeComponent },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '' }
]
