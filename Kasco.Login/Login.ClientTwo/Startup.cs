using Login.ClientTwo.Extensions;
using Login.ClientTwo.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Login.ClientTwo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void Configure(IApplicationBuilder application, IHostingEnvironment environment)
        {
            application.UseExceptionCustom(environment);
            application.UseCorsCustom();
            application.UseHstsCustom(environment);
            application.UseHttpsRedirection();
            application.UseStaticFiles();
            application.UseSpaStaticFiles();
            application.UseResponseCaching();
            application.UseMvcWithDefaultRoute();
            application.UseSpaCustom(environment);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddResponseCaching();
            services.AddMemoryCache();
            services.AddMvcCustom();
            services.AddSpaStaticFilesCustom();

            services.Configure<ClientAppSettings>(Configuration.GetSection("ClientAppSettings"));
        }
    }
}
